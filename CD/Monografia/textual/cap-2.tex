\chapter{Revisão Bibliográfica}
Este capítulo apresenta algumas pesquisas já realizadas que possuem temas abordados pelo trabalho, tratando sobre conceitos, definições e contexto histórico de sistemas tutores inteligentes, objetos de aprendizagem e ferramentas de autoria.

\section{Sistemas Tutores Inteligentes}
Sistemas tutores inteligentes (STIs) são softwares voltados à Educação que incorporam elementos de Inteligência Artificial (IA) para prover ao professor um relacionamento de conhecimentos específicos no método de aprendizagem. Esse tipo de sistema abrange as disciplinas de Ciência da Computação, Psicologia Cognitiva e teorias de aprendizagem educacional, formando assim uma área chamada de Ciência Cognitiva~\cite{Nwana1990}.

\begin{figure}[H]
	\conteudoFigura%
	[\citeonline{Silva2015}]
	{.8\textwidth}
	{diagramas/areas-sti.png}
	{Elementos constituintes de um STI}
	{fig:areas-sti}
\end{figure}

Por reunir os conhecimentos de três áreas distintas, o projeto de STIs oferece uma plataforma de testes interessante para cientistas da computação, psicólogos cognitivos e pesquisadores da área de educação~\cite{Nwana1990}. Além disso, outra vantagem da aplicação de STIs assemelha-se às vantagens do ensino particular ou individual, pois certos alunos obtêm maior desempenho em aulas particulares do que em aulas tradicionais.

Os pesquisadores \citeonline{Ma2014}, baseados nos estudos de \citeonline{Shute1994} e \citeonline{Sottilare2013}, definem o conceito de Sistema Tutor Inteligente como um sistema de computador que para cada estudante:
\begin{enumerate}
	\item Realiza funções de tutoria de modo a representar a informação a ser aprendida, fazendo questões ou definindo tarefas de aprendizagem, provendo \textit{feedback} ou dicas, e respondendo questões propostas pelo estudante.
	\item Computa respostas do estudante de forma a construir um modelo de seus estados psicológicos (como estratégias de aprendizado, motivações ou emoções).
	\item Emprega a modelagem identificada no item 2 para adaptar uma ou mais funções do item 1.
\end{enumerate}

	\subsection{Origens}
	As áreas de Inteligência Artificial e Educação estão associadas desde o início da década de 1970. Um dos primeiros sistemas tutores inteligentes foi desenvolvido por Jaime Carbonell em 1970, chamado SCHOLAR, o qual permitia aos alunos explorar características geográficas da América do Sul. Esse sistema percorria uma rede semântica de conhecimentos geográficos para gerar respostas individualizadas, em linguagem natural, a cada aluno que interagisse com o sistema. No SCHOLAR, surgiram os primeiros conceitos sobre módulos e arquitetura de STIs, pois algo que o diferenciava dos sistemas de ICAI (\textit{Intelligent Computer Assisted Instruction} --- Instrução Inteligente Assistida por Computador) da época era a maneira como sua aplicação representava o conhecimento do domínio separadamente da interface de linguagem natural. Esse módulo destinado somente à representação do domínio permitia ao sistema, em teoria, gerar um vasto e variado conjunto de questões, juntamente com respostas às perguntas propostas pelo aprendiz. Dessa forma, ficava enfatizado a possibilidade da representação de domínio servir como base para a modelagem do conhecimento do estudante~\cite{Ma2014, Woolf2009}.

	Já o primeiro STI baseado em um sistema especialista foi o GUIDON, criado por William Clancey, que ensinava conceitos médicos e que, futuramente, serviu como base para vários outros tutores da área de medicina~\cite{Woolf2009}.

	\subsection{Arquitetura}
	STIs variam bastante quanto à arquitetura, não havendo uma especificação bem estabelecida a esse respeito. Entretanto, desde a segunda metade da década de 1980, há um consenso de que esse tipo de sistema emprega pelo menos quatro módulos~\cite{Nwana1990, Silva2015, Shann2017}:
	\begin{itemize}
		\item módulo especialista;
		\item módulo do estudante;
		\item módulo tutor;
		\item módulo de interface.
	\end{itemize}

	\begin{figure}[H]
		\conteudoFigura%
		[\citeonline{Silva2015}]
		{.7\textwidth}
		{diagramas/arquitetura-sti.png}
		{Módulos de um STI}
		{fig:arquitetura-sti}
	\end{figure}

	O módulo especialista mantém o conhecimento específico relevante ao domínio da aplicação, ou seja, o assunto que será lecionado ao aluno. Esse módulo compreende as tarefas de elicitação e codificação do conhecimento, servindo também para representá-lo de várias formas como, por exemplo, na rede semântica utilizada pelo sistema SCHOLAR. Além disso, o módulo especialista possui outras duas funções, provendo o conhecimento que será apresentado ao aprendiz e capacitando a avaliação do conhecimento do estudante, por meio da geração de questões e fornecimento de \textit{feedback} às respostas.

	O módulo do estudante atua dinamicamente no aprendizado do aluno, com foco na coleta de dados sobre desempenho, cognição e capacidade de aprendizado. Alguns papéis desse módulo incluem a transferência dos dados coletados ao módulo especialista, aprimorando a representação do conhecimento do domínio; e a atuação como fonte de informação sobre o estudante, representando o conhecimento acerca deste.

	O gerenciamento das interações do estudante com o sistema é realizado pelo módulo tutor, monitorando as respostas que o estudante fornece para que o sistema adapte suas ações. Ademais, esse módulo pode permitir ao aluno o total controle sobre o sistema, restando ao módulo somente o papel de planejamento do ambiente de estudo e, se for o caso, modificação do contexto de aprendizagem.

	O último módulo é o módulo de interação, onde ocorre a interação do aluno com o sistema. Esse componente deve ser, preferencialmente, de fácil usabilidade e, de certa forma, atrativo ao usuário, agindo como um meio de apresentação de conhecimento do domínio ao estudante de forma acessível e compreensível.

	% \subsection{Modelagem do Aluno}
	% \subsection{Avaliação do Aprendiz}
	% \subsection{Exemplos de STIs}

\section{Objetos de Aprendizagem}
Objetos de aprendizagem (OAs) são geralmente definidos como entidades que podem ser distribuídas pela Internet e, assim, atingir várias pessoas, com potencial no âmbito de reusabilidade, adaptabilidade e escalabilidade. Levando em conta essas características, entende-se que a ideia fundamental de um objeto de aprendizagem é seu reuso em várias situações, permitindo ao professor construir pequenos componentes de aprendizagem que possam ser expandidos e reutilizados em diferentes contextos de aprendizagem, com possibilidade de se adaptar de aluno para aluno.

\citeonline{Wiley2002} aponta também que cada OA possui certas qualidades e, portanto, suas próprias classificações. Baseado nessa ideia, foi definida uma taxonomia específica para OAs:
\begin{itemize}
	\item Fundamental: um conceito de aprendizagem básico, como uma imagem ou texto.
	\item Combinação fechada: um tipo mais próximo da realidade, como um vídeo.
	\item Combinação aberta: combinação dos tipos fundamental e combinação fechada, como uma página web contendo imagens, vídeos e texto para ensino de um assunto específico.
	\item Gerador de apresentação: mais dinâmico e interativo, como um \textit{applet} Java.
	\item Gerador de instrução: no qual é fornecido e instruído a prática de qualquer tipo de procedimento, seja por meio de exercícios ou não.
\end{itemize}

Funcionalmente, têm-se discutido sobre os requisitos que um objeto de aprendizagem deve ter, os quais se assemelham às suas características~\cite{Polsani2006}:
\begin{itemize}
	\item Acessibilidade: mediante uso de metadados, deve proporcionar fácil acesso e armazenamento.
	\item Reusabilidade: deve funcionar em variados contextos de aprendizagem.
	\item Interoperabilidade: deve ser independente dos meios de distribuição e dos sistemas de gerenciamento de conhecimento.
\end{itemize}

Para um OA ser reutilizável, seu objetivo de uso não pode ser vinculado a como ele foi criado, pois ele poderá vir a ser utilizado em vários contextos de aprendizagem. As metodologias educacionais fornecem um \textit{framework} para que sejam construídos métodos de aprendizagem que atinjam certas metas no contexto educacional. Separar sua criação de sua implantação não favorecerá o uso de metodologias educacionais que, em um projeto não ideal, podem ser inerentes ao objeto e causar sua vinculação com determinada área. Assim, OAs alcançam aspectos que se relacionam à implantação em situações educacionais distintas para facilitar o aprendizado~\cite{Polsani2006}.

Ainda, \citeonline{Barritt2004} descrevem que, idealmente, um OA deve ser baseado em objetivo, livre de contexto, interativo, auto-descritivo, autônomo, de origem única e livre de formato. Isso significa que um OA deve ter um único objetivo de aprendizagem, com a função de atingir essa meta essencial. Preferencialmente que possa ser interativo e facilmente encontrado por meio de metadados descritivos. Com autonomia de conteúdo significa que o OA é independente de qualquer outro OA, professor ou sistema para que possa ser facilmente integrado junto a outros OAs. Também deve ser de origem única no sentido em que pode ser usado e reusado em múltiplos formatos, por múltiplos autores e em múltiplos ambientes educacionais, seja qual for sua forma de utilização. Para sua distribuição, OAs não devem estar vinculados a nenhum meio fixo para que esse possa ser facilmente redistribuído de diversas maneiras. Além disso, seu uso deve ser portável e não atrelado à nenhuma plataforma específica.

O modelo de objeto de aprendizagem reusável (\textit{reusable learning object}) que a Cisco Systems propõe envolve a definição de que um OA é direcionado a ensinar uma tarefa baseada em um único objetivo de aprendizado. Para fazer do OA uma completa experiência de aprendizagem, geralmente adiciona-se Visão Geral (parte teórica), Sumário (conclusão) e Avaliação (parte prática). A Visão Geral serve para situar o estudante sobre o tema da aula, contendo introdução, importância, objetivos, pré-requisitos, cenário e perfil. O Sumário é usado para concluir o OA e amarrar conceitualmente os objetivos abordados ao cenário da aula, possuindo os seguintes itens: revisão, próximas etapas e recursos adicionais. A Avaliação tem como objetivo garantir que o aprendiz atingiu os objetivos da aula e adquiriu o conhecimento necessário sobre a teoria proposta~\cite{Cisco2001}.

	\subsection{Metadados}
	Um aspecto importante dos objetos de aprendizagem é a capacidade de conter metadados, ou seja, um conjunto de dados que fornece informações sobre outros dados. Os metadados se referem a um conjunto de palavras-chave, atributos e dados descritivos que informam sobre os autores, aprendizes e sistemas~\cite{Barritt2004}. Dessa forma, esses dados são intrínsecos ao objeto de aprendizagem como, por exemplo, autor, título e data de publicação.

	A principal vantagem no emprego de metadados é ajudar alunos, professores e computadores a encontrarem OAs relevantes em suas respectivas áreas de interesse~\cite{Wiley2002}.  Aprendizes podem procurar por OAs que atendam às suas necessidades para auxiliar em seus estudos e professores podem buscar OAs de seu interesse para melhorar ou construir novas soluções de aprendizagem~\cite{Barritt2004}.

	No início do \textit{e-learning}, o reuso de recursos educacionais era escasso e a interoperabilidade entre os sistemas gerenciadores de aprendizado (LMS --- \textit{Learning Management System}) era baixa. Para isso, surgiu um esforço para padronizar e referenciar a criação e manutenção de objetos de aprendizagem. Assim, várias referências e padrões foram aparecendo, sendo que as duas mais utilizadas atualmente são o \textit{Shareable Content Object Reference Model} (SCORM) e o IEEE \textit{Learning Object Metadata} (LOM).

		\subsubsection{\textit{Shareable Content Object Reference Model} (SCORM)}
		Durante a transição de Treinamento Baseado em Computador (CBT --- \textit{Computer-Based Training}) para \textit{e-learning}, um dos maiores desafios foi o de estabelecer a interoperabilidade entre os sistemas, ou seja, se comunicar e trocar dados. Para coletar as informações de desempenho de aprendizagem sobre o estudante, o sistema tinha que ser especificamente programado de acordo com cada ambiente, elevando seus custos de manutenção e, eventualmente, diminuindo seu tempo de vida. Além disso, o reuso de componentes se tornava inviável, pois para cada ambiente geralmente tinham de ser desenvolvidos novos componentes e também podia ocorrer de alguns se tornarem inutilizados~\cite{ADL2015}.

		O SCORM (\textit{Shareable Content Object Reference Model} --- Modelo de Referência para Objetos de Conteúdo Compartilhável) surgiu com o propósito de sanar esses problemas. SCORM é um conjunto de referências e diretrizes para auxiliar na elaboração de materiais digitais de aprendizagem de modo interoperável e baseado em tecnologias web. Sua primeira versão foi lançada em 2000, sendo concebido pelo Advanced Distributed Learning (ADL), um grupo de pesquisa financiado pelo Departamento de Defesa dos Estados Unidos~\cite{ADL2015, Rustici2017}.

		A definição do que é SCORM pode ser considerada, como o nome diz, um ``modelo de referência'', portanto, não significando que SCORM é exatamente um padrão. Quando a ADL iniciou a escrita dos documentos SCORM, os pesquisadores notaram que a indústria já possuía alguns padrões estabelecidos que resolviam parte dos problemas. O SCORM referencia alguns desses padrões e define como deve ser o uso deles de forma conjunta~\cite{Rustici2017}.

		O SCORM propõe que um objeto de aprendizagem gerado em um LMS compatível com SCORM seja independente da plataforma na qual será utilizado e que a transferência dos OAs entre LMSs seja simples e fácil. Além disso, contribui com uma série de padrões para que os softwares de \textit{e-learning} sejam escritos de forma que possam se integrar a outros softwares educativos~\cite{Rustici2017-2}.

		Todas as versões do SCORM especificam principalmente duas características: empacotamento de conteúdo (portabilidade e reusabilidade) e troca de dados em tempo de execução (interoperabilidade). O empacotamento se refere a como o LMS importa e executa o conteúdo sem intervenção humana, através de um arquivo XML (\textit{eXtensible Markup Language}). A comunicação em tempo de execução é como o conteúdo se comunica com o LMS enquanto este estiver sendo executado, com distribuição e rastreamento do conteúdo, com ações como requisitar o nome do estudante e informar ao LMS que ``o estudante respondeu 95\% das questões''~\cite{Rustici2017-2}.

		Além disso, o conteúdo desenvolvido em conformidade com SCORM é independente de contexto, ou seja, funcionará em situações variadas, seja inserido em um ambiente de gerenciamento de aprendizagem ou como parte de um curso \textit{on-line} publicado diretamente na web ou ainda em cenário híbrido~\cite{Dutra2006}.

		A especificação SCORM é direcionada para o conteúdo do OA e na maneira como ele deve ser criado de modo a ser automatizado. Porém, não se aprofunda em outras questões do ensino, ficando restrito à interação direta do aluno com o OA~\cite{Dutra2006}.

		Outro conceito importante de se ressaltar é o Objeto de Conteúdo Compartilhável (SCO --- \textit{Shareable Context Object}), que o SCORM define e constantemente aborda. Um SCO é um objeto com a menor granularidade possível no contexto SCORM, podendo ser um módulo, um capítulo, uma página, entre outros. Teoricamente, um SCO deve ser o menor fragmento de componente educacional, sendo reusável e independente.

		Atualmente, o SCORM consiste de três publicações de especificação técnica que abordam portabilidade, reusabilidade, durabilidade e sequenciamento. Sobre portabilidade, o livro chamado \textit{SCORM Content Aggregation Model} (CAM)~\cite{SCORM-CAM-2009} estabelece como empacotar conteúdo de acordo com cada sistema em um arquivo ZIP chamado \textit{Package Interchange Format} (PIF). O livro também aborda a descrição de componentes para proporcionar fácil busca com foco na reusabilidade através de metadados. Para tratar da interoperabilidade e portabilidade, o \textit{SCORM Run-time Environment} (RTE)~\cite{SCORM-RTE-2009}, é o livro que define um modelo de dados comum e uma interface de programação de aplicativos (API) para conteúdos de \textit{e-learning}. Isso proporciona uma comunicação padrão entre o lado cliente e o componente de sistema (\textit{``run-time environment''}), sendo este último geralmente provido por um sistema gerenciador de aprendizagem. Já o livro \textit{SCORM Sequencing and Navigation} (SN)~\cite{SCORM-SN-2009} apresenta diretrizes de distribuição de conteúdo para aprendizes por meio de uma série de eventos de navegação, especificando também a sequência em que cada componente pode ser apresentado.

		\subsubsection{\textit{Learning Object Metadata} (LOM)}
		O padrão IEEE 1484.12.1-2002~\footnote{\url{https://ieee-sa.imeetcentral.com/ltsc}} LOM estabelece um esquema de dados cujos elementos compõem uma instância de um metadado sobre um determinado recurso educacional. Esse documento especifica mais de 70 elementos, os quais são divididos em categorias, mas nem todos sendo obrigatórios. Dentre alguns dos metadados das categorias principais citam-se: autor, título, palavras-chave, descrição, versão, linguagem, formato, dificuldade, custo, data e objetivo~\cite{LOM2002}.

		O principal objetivo que essa especificação visa atingir é o de aprimorar a descrição de um recurso educacional para que este seja fácil de achar, avaliar, obter e usar entre estudantes, professores e softwares educacionais através do emprego de metadados.

		O documento especifica também alguns requisitos de utilização, como o provimento de dados ou serviços, entendimento das necessidades do usuário para representação por meio de um perfil de aplicação, estratégia para criar metadados de alta qualidade e possibilidade de transferir dados com outros sistemas facilmente~\cite{Barker2005}.

	\subsection{Repositórios}
	Conforme a utilização de recursos educacionais digitais foi avançando, surgiu a necessidade de organização desses recursos de alguma forma em que várias pessoas pudessem consultar. Com a evolução da web, percebeu-se que os \textit{websites} poderiam agir como bibliotecas digitais, possibilitando aos professores compartilhar, gerenciar e usar esses materiais. Alguns desses repositórios são:
	\begin{itemize}
		\item Wisc-Online~\footnote{\url{https://www.wisc-online.com}}: criado em 2000, é um repositório multidisciplinar de objetos de aprendizagem, sendo um esforço colaborativo do \textit{Wisconsin Technical College System (WTCS)}.
		\item Ariadne~\footnote{\url{http://www.ariadne-eu.org}}: uma associação europeia que atua compartilhando recursos educacionais desde 1996.
		\item MERLOT~\footnote{\url{https://www.merlot.org}}: formado pela Universidade do Estado da Califórnia, em 1997, contando com objetos de aprendizagem de várias áreas.
	\end{itemize}

\section{Ferramentas de Autoria}
As ferramentas de autoria buscam integrar o conhecimento técnico de professores à informatização do ensino. Também procuram facilitar a criação de recursos educacionais através da disponibilização rápida e eficaz para aprendizes.

Segundo \citeonline{W3C2015}, ferramenta de autoria é uma aplicação ou um conjunto de aplicações, web ou não, empregada por autores de conteúdo para criar ou manipular conteúdo web para ser usado por outras pessoas. São considerados exemplos de ferramentas de autoria:
\begin{itemize}
	\item Editores HTML (\textit{Hypertext Markup Language}) WYSIWYG (\textit{What You See Is What You Get});
	\item Softwares que permitem a edição direta do código fonte;
	\item Ambientes de desenvolvimento integrado (IDEs);
	\item Softwares para a edição rápida de partes de páginas web;
	\item Ferramentas destinadas à criação de aplicativos móveis.
\end{itemize}

Ainda, esse tipo de ferramenta possui suporte à identificação e padronização mediante uso de metadados nos OAs. Isso para que os recursos gerados possam ser facilmente encontrados, reusados e adaptados~\cite{Bez2010}.

Para \citeonline{Murray2003}, ferramentas de autoria devem suportar características similares às características de objetos de aprendizagem:
\begin{itemize}
	\item Interoperabilidade: responde à questão se o sistema consegue operar juntamente a outros sistemas.
	\item Gerenciabilidade: se o sistema é capaz de capturar e localizar informações sobre o aprendiz e sobre a sessão de estudo.
	\item Reusabilidade: se o material didático gerado pode ser reutilizado em outros contextos diferentes do qual foi concebido.
	\item Durabilidade e escalabilidade: se a ferramenta permanecerá utilizável conforme a evolução de tecnologias e padrões. Uma solução é o desenvolvimento de sistemas extensíveis com o uso de tecnologias de código aberto ou através de uma arquitetura componentizada.
	\item Acessibilidade: fornecimento de material de forma fácil e em tempo razoável.
\end{itemize}

Existem algumas categorias de ferramentas de autoria de propósito geral que surgiram, como o WEAR (\textit{Web Based authoring tool for Algebra Related domains})~\cite{Adenowo2014}, onde se encaixa a ADAPTFARMA. Uma das vantagens sobre as ferramentas desse tipo é a fácil disponibilização dos recursos criados. Por se tratar de conteúdo web, o aprendiz pode usar o produto da ferramenta em qualquer hora e de qualquer dispositivo com acesso à Internet. Além disso, o criador pode modificar o software e distribuí-lo novamente para os professores poderem atualizar seus sistemas sem grande dificuldade.

\citeonline{Isotani2004} desenvolveram um sistema para aprendizado de Geometria com correção automática de exercícios, o iGeom~\footnote{\url{http://www.matematica.br/igeom}}. Os principais objetivos desse software eram detectar soluções corretas, fornecer \textit{feedback} sobre a avaliação imediatamente e, se for o caso, exibir um contraexemplo ao aluno. Um desafio observado na correção de exercícios da ferramenta foi a possibilidade de o aluno resolvê-lo de maneira totalmente diferente da maneira proposta pelo professor, possivelmente utilizando técnicas não usuais. A solução foi apresentar ao aluno um gabarito (feito pelo professor) onde estão os objetos iniciais para descrição do problema. Assim, a resposta do aprendiz é avaliada com base em seu gabarito e no gabarito do professor.

\citeonline{Borges2003} criaram uma ferramenta de autoria que trabalha com um grafo conceitual. O professor-autor indica se um conceito ``é um'', ``é parte'' ou ``é pré-requisito'' de outro conceito. A partir daí o algoritmo desenvolvido estabelece relacionamentos entre os conceitos e determina uma sequência adequada com base nas indicações do professor.

	\subsection{Ferramentas de Autoria para Sistemas Tutores Inteligentes}
	Considerando que os sistemas tutores inteligentes vêm se tornando importantes no cenário educacional, ainda é complicado e caro de desenvolvê-los. Algumas ferramentas de autoria estão disponíveis comercialmente para instrução assistida por computador ou aprendizado baseado em recursos multimídia, mas ainda assim essas ferramentas não são totalmente capazes de gerar sistemas tutores inteligentes, seja por questão de tecnologia ou pela vaga representação de teorias pedagógicas e educacionais~\cite{Murray1999}. Conforme \citeonline{Adenowo2014} uma ferramenta de autoria de STIs é ``um software designado a gerar sistemas inteligentes de tutoria para suporte educacional''.

	Para \citeonline{Murray1999}, existem sete categorias de ferramentas de autoria de STIs, conforme a tabela~\ref{tab:categorias-fas}.

	\begin{table}[H]
		\ABNTEXfontereduzida%
		\centering
		\caption{Categorias de ferramentas de autoria de STIs\label{tab:categorias-fas}}
		\begin{tabulary}{\textwidth}{LLLL}
			\toprule
			\textbf{Categoria} & \textbf{Vantagens} & \textbf{Limitações} & \textbf{Exemplos} \\
			\toprule
			Planejamento de currículo e sequenciamento & Regras, restrições ou estratégias para sequenciar módulos & Vaga representação de habilidades & DOCENT, IDE, ISD Expert \\ \midrule
			Estratégias de tutoria & Múltiplas estratégias de ensino; conjunto sofisticado de primitivas educacionais & (Mesmas limitações da categoria acima) & Eon, GTE, REDEEM \\ \midrule
			Simulação de dispositivos e treinamento de equipamento & Autoria e tutoria correspondentes à identificação e solução de problemas do dispositivo em questão & Estratégias educacionais e modelagem do estudante limitadas & DIAG, RIDES, SIMQUEST \\ \midrule
			Sistema especialista / tutor cognitivo & Sólida modelagem do estudante & Difícil de desenvolver; estratégias educacionais limitadas & Demonstr8, D3 Trainer, Training Express \\ \midrule
			Múltiplos tipos de conhecimento & Clara representação de fatos, conceitos e procedimentos & Estratégias educacionais pré-definidas & CREAM-Tools, DNA, ID-Expert \\ \midrule
			Propósito especial & Princípios pedagógicos e projeto bem fundamentados & Cada ferramenta é limitada a um tipo específico de sistema & IDLE-Tools/IMap, LAT \\ \midrule
			Hipermídia inteligente/adaptativa & Acessibilidade e uniformidade de interface & Interatividade limitada; difícil sequenciamento de conteúdos & CALAT, GETMAS, InterBook \\
			\bottomrule
		\end{tabulary}
		\legend{Fonte: \citeonline{Murray1999}}
	\end{table}

	Apesar da classificação bem definida, \citeonline{Murray1999} conclui que várias das ferramentas de autoria existentes possuem características combinadas, sendo isso um ponto positivo, pois pode atenuar suas limitações e melhorar a interação do aluno com o sistema.

	Como exemplo, o Eon foi uma suíte de ferramentas de autoria voltado para a criação de sistemas tutores inteligentes, suportando os quatro módulos clássicos de STIs. Este sistema permitia ao professor-autor editar um grafo dos tópicos do STI, editar as estratégias de ensino, interações, modificar o modelo do estudante, o conteúdo, além de ter um visualizador de documentos e permitir busca de tópicos, tudo através de uma interface gráfica. Seu desenvolvimento foi finalizado em 1997~\cite{Murray1996}.

	A ferramenta de autoria ADAPTFARMA é focada em produção de objetos de aprendizagem. Também permite a aprendizes utilizarem o OA criado e auxilia o professor-autor a acompanhar os alunos envolvidos no processo de aprendizagem.

	\subsection{Ferramentas de Autoria para Sistemas Adaptativos de Hipermídia}
	Um Sistema Adaptativo de Hipermídia (AHS --- \textit{Adaptive Hypermedia System}) pertence à classe de sistemas que se adaptam ao usuário. Uma característica importante de um AHS é a capacidade de reter informações a cerca do usuário, como conhecimento, objetivos e interesses para fornecer um ambiente individual, variando conforme seus objetivos individuais~\cite{Brusilovsky2012}.

	\citeonline{Brusilovsky2003} enfatiza que para a concepção de um AHS deve-se considerar dois estágios: projeto e autoria. O estágio de projeto deve conter as seguintes tarefas:
	\begin{itemize}
		\item Projetar e estruturar o espaço de conhecimento;
		\item Projetar um modelo de usuário genérico;
		\item Projetar um conjunto de objetivos de aprendizagem;
		\item Projetar e estruturar o hiperespaço de material educacional (topologia das conexões entre as páginas conceituais);
		\item Projetar as conexões entre o espaço de conhecimento e o hiperespaço de material educacional.
	\end{itemize}

	Já para a etapa de autoria, consideram-se as tarefas:
	\begin{itemize}
		\item Criar a página de conteúdo;
		\item Definir vínculos entre as páginas;
		\item Criar alguma descrição de cada elemento de conhecimento;
		\item Definir vínculos entre os elementos de conhecimento;
		\item Definir vínculos entre os elementos de conhecimento e página com material educacional.
	\end{itemize}

	Sobre o último passo da parte de projeto de um AHS, \citeonline{Brusilovsky2003} complementa que o processo de conexão entre o espaço de conhecimento e o hiperespaço de material educacional também é conhecido como indexação ou classificação, pois especificar um conjunto de conceitos fundamentais para cada página de material educacional é muito similar a classificar uma página de conteúdo com um conjunto de palavras-chave. Há quatro aspectos que distinguem as diferentes abordagens de indexação: cardinalidade, granularidade, navegação e poder expressivo.

	Para o aspecto de cardinalidade, há dois casos diferentes: indexação de conceito único, onde um fragmento do material educacional é relacionado a um, e somente um, conceito do domínio, e indexação de múltiplos conceitos, onde cada fragmento pode ser relacionado a vários conceitos. A indexação de conceito único é mais simples e mais intuitiva, enquanto a indexação de múltiplos conceitos é mais complexa e necessita de um nível de conhecimento maior.

	Granularidade se refere à precisão da indexação, como indexar a página inteira a conceitos, indexar fragmentos da página a conceitos, ou mesmo indexar um conjunto de páginas com um conjunto de conceitos.

	O aspecto da navegação diferencia quando um vínculo entre conceito e página está só em nível conceitual ou também quando define um caminho a ser seguido.

	O poder expressivo relaciona-se com a quantidade de informação que o autor consegue transmitir através dos vínculos entre páginas e conceitos. Alguns sistemas costumam atrelar pesos a esses vínculos de forma a diferenciar os tipos de conexões entre páginas e conceitos como, por exemplo, distinguir a parte introdutória de um resumo ou síntese. Outros sistemas utilizam de pré-requisitos para marcar quando um conceito não está na página, mas é necessário ser exibido para que o aprendiz entenda o conteúdo da página.

	A abordagem mais conhecida para indexação de vários conceitos é a indexação de páginas. Uma forma de indexação de página é adicionar a função de cada conceito no índice da página. Um exemplo de função seria o de pré-requisito, no qual o conceito é incluído no índice da página se o aprendiz deve saber esse conceito para entender o conteúdo da página. Pesos também podem ser adicionados às páginas para mostrarem o quanto uma página conceitual contribui para o aprendizado do estudante.

	Outra abordagem chama-se indexação de fragmentos, onde um fragmento da página se relaciona a vários conceitos, que pode ocorrer de mais de um fragmento se relacionar ao mesmo conceito. O sistema MetaDoc aplica essa prática decidindo mostrar ou não o fragmento de texto presente na página dependendo do nível de conhecimento do usuário. Assim, o sistema mostrará poucos fragmentos adicionais e mais detalhes se o usuário possuir um nível de conhecimento alto sobre os conceitos indexados e mostrará todos os fragmentos adicionais e poucos detalhes caso contrário --- algo entediante ao primeiro usuário.

	Considerando esses aspectos, utiliza-se uma técnica chamada de anotação para marcar o estado atual do conceito que está sendo apresentado ou para marcar que uma página conceitual ainda não está pronta para ser exibida, significando que seus pré-requisitos não foram atendidos. Também pode-se decidir por não mostrar ao aprendiz páginas com pré-requisitos ainda não atendidos e páginas que não correspondem ao atual objetivo educacional.
